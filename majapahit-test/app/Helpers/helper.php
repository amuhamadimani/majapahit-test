<?php

function getItem($id){
	$item = App\Models\Item::select('nama_item','harga', 'points')->where('id', $id)->first();
	return $item;	
}

function getUser($id){
	$user = App\User::select('name','points')->where('id', $id)->first();
	return $user;	
}
