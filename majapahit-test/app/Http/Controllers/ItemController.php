<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = Item::all();
        return view('admin.item.index', ['item' => $item]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.item.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong.',
        ];

        $request->validate([
            'nama_item' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'gambar' => 'required',
            'detail' => 'required',
            'tipe' => 'required',
        ],$messages);

        $item = new Item();

        $item_image = $request->file('gambar');
        $file_name_item_image = rand() . '.' . $item_image->getClientOriginalExtension();

        $full_path_item_image = $file_name_item_image;
        $item_image->move(public_path('uploads/item/'), $file_name_item_image);

        $item->nama_item = $request->nama_item;
        $item->harga = $request->harga;
        $item->stok = $request->stok;
        $item->tipe = $request->tipe;
        $item->detail = $request->detail;
        $item->gambar = $full_path_item_image;
        $item->points = $request->points;

        if($item->save()){
            return redirect()->route('item.index')->with(['success-message' => 'Berhasil Menyimpan Data.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        return view('admin.item.edit', ['item' => $item]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong.',
        ];

        $request->validate([
            'nama_item' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'gambar' => 'required',
            'detail' => 'required',
            'tipe' => 'required',
        ],$messages);

        $item = Item::find($id);

        $item_image = $request->file('gambar');
        $file_name_item_image = rand() . '.' . $item_image->getClientOriginalExtension();

        $full_path_item_image = $file_name_item_image;
        $item_image->move(public_path('uploads/item/'), $file_name_item_image);

        $item->nama_item = $request->nama_item;
        $item->harga = $request->harga;
        $item->stok = $request->stok;
        $item->tipe = $request->tipe;
        $item->detail = $request->detail;
        $item->gambar = $full_path_item_image;
        $item->points = $request->points;

        if($item->save()){
            return redirect()->route('item.index')->with(['success-message' => 'Berhasil Menyimpan Data.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        if($item->delete()){
            return back()->with(['error-message' => 'Berhasil Menghapus Data.']);
        }
    }
}
