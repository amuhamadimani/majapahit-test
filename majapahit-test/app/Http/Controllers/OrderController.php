<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Item;
use App\Models\Transaksi;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = Item::where('tipe', 1)->get();
        $a = Transaksi::select('id')->latest()->value('id');
        $id = $a +1;
        $id_order = 'OR'.date('dmY').$id;

        return view('admin.transaksi.order.index', ['item' => $item, 'id_order' => $id_order]);
    }

    public function keranjang($id)
    {
        $order = Order::where('id_order', $id)->get();
        $id_order = Order::where('id_order', $id)->first();
        $total = Order::where('id_order', $id)->sum('qty');

        return view('admin.transaksi.order.keranjang', ['order' => $order, 'total' => $total, 'id_order' => $id_order]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $a = Transaksi::select('id')->latest()->value('id');
        $id = $a +1;
        $id_order = 'OR'.date('dmY').$id;

        $date = date('Ymd');

        for($i = 0; $i < count($request->qty); $i++){
            if($request->qty [$i] != null){
                $order = new Order();

                $order->id_order = $id_order;
                $order->id_item = $request->id_item[$i];
                $order->qty = $request->qty[$i];
                $order->tanggal = $date;
                
            }
        }

        if($order->save()){
            return redirect()->route('order.index')->with(['success-message' => 'Item Berhasil Ditambahkan.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id_user = \Auth::user()->id;
        $point = \Auth::user()->points;
        $date = date('Y-m-d');

        $messages = [
            'required' => ':attribute tidak boleh kosong.',
        ];

        $request->validate([
            'total_bayar' => 'required',
        ],$messages);

        $order = new Transaksi();
        
        $order->id_order = $request->id_order;
        $order->id_user = Auth::user()->id;
        $order->total_bayar = $request->total_bayar;
        $order->tanggal = $date;
        $order->tipe_transaksi = 1;
        $order->status = 1;
        $order->save();

        $user = User::where('id',$id_user)->first();
        $user->points = $point + 5;
        $user->save();

        return redirect()->route('transaksi.index')->with(['success-message' => 'Item Berhasil Ditambahkan.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
