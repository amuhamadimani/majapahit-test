<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaksi;
use App\User;
use App\Models\Item;
use App\Models\Order;

class PointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = Item::where('tipe', 2)->get();
        $a = Transaksi::select('id')->latest()->value('id');
        $id = $a +1;
        $id_order = 'OR'.date('dmY').$id;

        return view('admin.transaksi.point.index', ['item' => $item, 'id_order' => $id_order]);
    }

    public function keranjang($id)
    {
        $order = Order::where('id_order', $id)->get();
        $id_order = Order::where('id_order', $id)->first();
        $total = Order::where('id_order', $id)->sum('qty');

        return view('admin.transaksi.order.keranjang', ['order' => $order, 'total' => $total, 'id_order' => $id_order]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $qty = $request->qty;
        $point = $request->point;
        $points = $point * $qty;

        $point_user = \Auth::user()->points;
        $id_user = \Auth::user()->id;
        
        $a = Transaksi::select('id')->latest()->value('id');
        $id = $a +1;
        $id_order = 'OR'.date('dmY').$id;

        $date = date('Ymd');
        
        $point = \Auth::user()->points;
    
        if($point_user < $points){
            return redirect()->route('point.index')->with(['error-message' => 'Point anda kurang, silahkan belanja lagi dan dapatkan lebih banyak poin']);
        }

        $order = new Order();

        $order->id_order = $id_order;
        $order->id_item = $request->id_item;
        $order->qty = $request->qty;
        $order->tanggal = $date;
        $order->save();

        $user = User::where('id', $id_user)->first();
        $user->points = $point_user - $points;
        $user->save();

        return redirect()->route('point.index')->with(['success-message' => 'Item Berhasil Ditambahkan.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong.',
        ];

        $request->validate([
            'total_bayar' => 'required',
        ],$messages);

        $order = new Transaksi();
        
        $order->id_order = $request->id_order;
        $order->id_user = \Auth::user()->id;
        $order->total_point = $request->total_point;
        $order->tipe_transaksi = 2;
        $order->status = 1;

        if($order->save()){
            return redirect()->route('transaksi.index')->with(['success-message' => 'Transaksi Berhasil.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
