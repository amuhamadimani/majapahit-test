<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return view('admin.user.index', ['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong.',
        ];

        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'password' => 'required',
            'status' => 'required',
        ],$messages);

        $user = new User();

        $user->name = $request->nama;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->status = $request->status;

        if($user->save()){
            return redirect()->route('user.index')->with(['success-message' => 'Berhasil Menyimpan Data.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.user.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong.',
        ];

        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'password' => 'required',
            'status' => 'required',
        ],$messages);

        $user = User::find($id);

        $user->name = $request->nama;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->status = $request->status;

        if($user->save()){
            return redirect()->route('user.index')->with(['success-message' => 'Berhasil Mengubah Data.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = User::find($id);
        if($berita->delete()){
            return back()->with(['error-message' => 'Berhasil Menghapus Data.']);
        }
    }
}
