$(document).ready(function () {
    var closeCollapse = $('.close-collapse');
    var btnMenuMobile = $('.btn-menu-mobile');

    closeCollapse.on('click', function(){
        btnMenuMobile.trigger('click');
    });

});

$(window).on('load', function(){ 
    
    var scrollTop = 0;

    $(".loader").fadeOut("slow");

    $(window).scroll(function(){
        scrollTop = $(window).scrollTop();

        if (scrollTop >= 100) {
            $('#navbar-menu').addClass('scrolled-nav');
        } else if (scrollTop < 100) {
            $('#navbar-menu').removeClass('scrolled-nav');
        } 
    });
});

function convertToRupiah(angka)
{
    var rupiah = '';        
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    return 'Rp '+rupiah.split('',rupiah.length-1).reverse().join('');
}