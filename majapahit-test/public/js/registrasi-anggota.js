$(document).ready(function(){

    var data_input_kabupaten = {
        id_provinsi:$("#old_id_provinsi").val(),
        id_kabupaten:$("#old_id_kabupaten").val() 
    }

    console.log(data_input_kabupaten)

    getKabupatenAjax(data_input_kabupaten)


    $.ajaxSetup({
       headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
     });

    $.fn.getKabupaten = function(id_provinsi){

        var data = {
            id_provinsi:id_provinsi,
            id_kabupaten:$("#old_id_kabupaten").val()
        }

        getKabupatenAjax(data)

    }

    function getKabupatenAjax(data){

        if(data.id_provinsi != ""){
            $.ajax({
                type:'GET',
                url:'/get-kabupaten',
                data:data,
                success:function(result) {
                    $("#inputKabupaten").html(result.data);
                }
            });
        }

    }

})