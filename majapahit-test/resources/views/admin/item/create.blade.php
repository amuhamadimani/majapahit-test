@extends('layouts.admin.master')

@section('content')
    <!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">User Create</h1>
		<a href="{{ route('item.index') }}" class="btn btn-sm btn-primary shadow-sm btn-responsive"><i class="fas fa-arrow-circle-left fa-sm text-white-50"></i> Kembali</a>
	</div>

	<div class="row mb-4">
		<div class="col-md-12">
		  <div class="card shadow">
		    <div class="card-header py-3">
		      <h6 class="m-0 font-weight-bold text-primary">Input Data Item</h6>
		    </div>
		    <div class="card-body">
		    	<div class="row">
					<div class="col-md-12">
						@include('pages-message.form-submit')
						@include('pages-message.notify-msg-error')
						@include('pages-message.notify-msg-success')
						<form action="{{ route('item.store') }}" method="post" enctype="multipart/form-data" id="form-user">
							@csrf
							<div class="row">
								<div class="col-md-6">
							
									<div class="form-group">
										<label>Nama Item <span class="text-danger">*</span></label>
										<input type="text" class="form-control col-md-12" data-msg-required="Wajib diisi" required name="nama_item">
									</div>
									<div class="form-group">
										<label>Harga <span class="text-danger">*</span></label>
										<input type="number" class="form-control col-md-12" data-msg-required="Wajib diisi" required name="harga">
                                    </div>
									<div class="form-group">
										<label>Stok <span class="text-danger">*</span></label>
                                        <input type="number" class="form-control col-md-12" data-msg-required="Wajib diisi" required name="stok">
									</div>
									<div class="form-group">
										<label>Gambar <span class="text-danger">*</span></label>
                                        <input type="file" class="form-control col-md-12" data-msg-required="Wajib diisi" required name="gambar">
									</div>
                                </div>
                                <div class="col-md-6">

									<div class="form-group">
                                        <label>Detail <span class="text-danger">*</span></label>
                                        <textarea id="" cols="30" rows="4"class="form-control col-md-12" data-msg-required="Wajib diisi" required name="detail"></textarea>
                                    </div>
									<div class="form-group">
										<label>Tipe <span class="text-danger">*</span></label>
                                        <select id="" class="form-control col-md-12" data-msg-required="Wajib diisi" required name="tipe">
                                            <option value="">-- Tipe Barang --</option>
                                            <option value="1">Jual</option>
                                            <option value="2">Hadiah</option>
                                        </select>
                                    </div>
									<div class="form-group">
										<label>Points <span class="text-danger">*</span></label>
                                        <input type="number" class="form-control col-md-12" data-msg-required="Wajib diisi" required name="points">
                                    </div>
                                    
									<div class="form-group">
										<input type="submit" class="btn btn-success float-right" value="Save">
									</div>
                                </div>
							</div>
						</form>
					</div>
				</div>
		      
		    </div>
		  </div>
		</div>
	</div>
@endsection