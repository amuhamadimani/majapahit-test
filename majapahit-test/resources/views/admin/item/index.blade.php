@extends('layouts.admin.master')

@section('content')
    <!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Items</h1>
		<a href="{{ route('item.create') }}" class="btn btn-sm btn-primary shadow-sm btn-responsive"><i class="fas fa-plus fa-sm text-white-50"></i> Create</a>
	</div>

	<div class="row mb-4">
		<div class="col-md-12">
			@include('pages-message.form-submit')
			@include('pages-message.notify-msg-error')
			@include('pages-message.notify-msg-success')
		</div>
		<div class="col-md-12">
		  <div class="card shadow">
		    <div class="card-header py-3">
		      <h6 class="m-0 font-weight-bold text-primary">Data Items</h6>
		    </div>
		    <div class="card-body" style="overflow: auto;">
		      <table class="table table-hover" id="listdata">
		        <thead>
		            <tr>
		             	<th scope="col">No</th>
						<th scope="col">Nama Item</th>
						<th scope="col">Harga</th>
						<th scope="col">Stok</th>
						<th scope="col">Gambar</th>
						<th scope="col">Detail</th>
						<th scope="col">Points</th>
						<th scope="col">Tipe</th>
						<th scope="col">Action</th>
		            </tr>
                  </thead>
                  <tbody>
                      @foreach ($item as $val)
                          <tr>
                              <td>{{ $loop->index+1 }}</td>
                              <td>{{ $val->nama_item }}</td>
                              <td>Rp. {{ number_format($val->harga) }}</td>
                              <td>{{ $val->stok }}</td>
                              <td><img src="{{ asset('uploads/item/'.$val->gambar) }}" alt="" width="100" height="100"></td>
                              <td>{{ $val->detail }}</td>
                              <td>{{ $val->points }}</td>
                              <td>
                                  @php
                                      if($val->tipe == 1){
                                          echo "Barang Jual";
                                      }else{
                                          echo "Hadiah";
                                      }
                                  @endphp
                              </td>
                              <td>
                                  <a href="{{ url('admin/item/edit/'.$val->id) }}" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                  <a href="{{ url('admin/item/destroy/'.$val->id) }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                              </td>
                          </tr>
                      @endforeach
                  </tbody>
		      </table>
		    </div>
		  </div>
		</div>
	</div>
@endsection