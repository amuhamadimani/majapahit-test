@extends('layouts.admin.master')

@section('content')
    <!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Transaksi | Point : <span style="color: red">{{ \Auth::user()->points }}</span></h1>
		<div class="form-group">
			<a href="{{ route('transaksi.index') }}" class="btn btn-sm btn-warning shadow-sm btn-responsive"><i class="fas fa-certificate"></i> Tukar Point</a>
			<a href="{{ route('item.create') }}" class="btn btn-sm btn-primary shadow-sm btn-responsive"><i class="fas fa-shopping-cart"></i> Order</a>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-md-12">
			@include('pages-message.form-submit')
			@include('pages-message.notify-msg-error')
			@include('pages-message.notify-msg-success')
		</div>
		<div class="col-md-12">
		  <div class="card shadow">
		    <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Order</h6>
			    <a href="{{ route('transaksi.index') }}" class="btn btn-sm btn-primary shadow-sm btn-responsive float-right"><i class="fas fa-arrow-circle-left fa-sm text-white-50"></i> Kembali</a>
                
            </div>
            <div class="card-body" style="overflow: auto;">
                <a href="{{ url('admin/transaksi/order/'. $id_order) }}" class="btn btn-info float-right"><i class="fas fa-shopping-cart"></i> Keranjang</a>
                
                    <form action="{{ route('order.store') }}" method="post">
                        @csrf
                        <div class="row">
                            @foreach ($item as $val)
                                <div class="col-md-3">
                                    <div class="card text-center">
                                        <div class="card-header">
                                            <h5>{{ $val->nama_item }}</h5>
                                        </div>
                                        <div class="card-body" >
                                            <img src="{{ asset('uploads/item/'.$val->gambar) }}" alt="" width="200" height="150">
                                            <input type="hidden" name="id_item[]" value="{{ $val['id'] }}">
                                        </div>
                                        <div class="card-footer">
                                            <label for="">Rp. {{ number_format($val->harga,2) }}</label>
                                            <br>
                                            <input type="number" value="qty" class="form-control" name="qty[]">
                                            <br>
                                            <button class="btn btn-primary"><i class="fas fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </form>
		    </div>
		  </div>
		</div>
	</div>
@endsection