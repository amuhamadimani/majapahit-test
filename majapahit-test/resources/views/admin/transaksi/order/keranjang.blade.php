@extends('layouts.admin.master')

@section('content')
    <!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Transaksi | Point : <span style="color: red">{{ \Auth::user()->points }}</span></h1>
		<div class="form-group">
			<a href="{{ route('transaksi.index') }}" class="btn btn-sm btn-warning shadow-sm btn-responsive"><i class="fas fa-certificate"></i> Tukar Point</a>
			<a href="{{ route('item.create') }}" class="btn btn-sm btn-primary shadow-sm btn-responsive"><i class="fas fa-shopping-cart"></i> Order</a>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-md-12">
			@include('pages-message.form-submit')
			@include('pages-message.notify-msg-error')
			@include('pages-message.notify-msg-success')
		</div>
		<div class="col-md-12">
		  <div class="card shadow">
		    <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Order</h6>
	      	  <a href="{{ route('order.index') }}" class="btn btn-sm btn-primary shadow-sm btn-responsive float-right"><i class="fas fa-arrow-circle-left fa-sm text-white-50"></i> Kembali</a>
            </div>
            <div class="card-body" style="overflow: auto;">
                <table class="table table-hover" id="listdata">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">ID Order</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Harga Barang</th>
                            <th scope="col">Qty</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($order as $val)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $val->id_order }}</td>
                                <td>{{ getItem($val->id_item)->nama_item }}</td>
                                <td>Rp. {{ number_format(getItem($val->id_item)->harga) }}</td>
                                <td>{{ $val->qty }}</td>
                            </tr>
                        @endforeach
                      </tbody>
                  </table>

                  <form action="{{ url('admin/transaksi/order/update/'.$id_order->id_order) }}">
                    @csrf
                    <input type="hidden" name="id_order" value="{{ $id_order->id_order }}">
                    <div class="row">
                        <div class="col-md-2">
                              <label for="">Total Bayar</label>
                              <label for="">:</label>
                        </div>
                        <div class="col-md-2">
                          <input type="text" name="total_bayar" class="form-control" value="{{ getItem($id_order->id_item)->harga * $total }}" id="total" readonly>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-2">
                              <label for="">Bayar</label>
                              <label for="">:</label>
                        </div>
                        <div class="col-md-2">
                          <input type="text" class="form-control" value="" id="bayar">
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-2">
                              <label for="">Kembalian</label>
                              <label for="">:</label>
                        </div>
                        <div class="col-md-2">
                          <input type="text" class="form-control" value="" id="kembalian" readonly>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <button class="btn btn-success btn-lg btn-block">Bayar</button>
                        </div>
                    </div>
                  </form>

                  
		    </div>
		  </div>
		</div>
	</div>
@endsection


@section('js')
    <script>
        $('#bayar').keyup(function(){
            var total = parseInt($('#total').val()); 
            var bayar = parseInt($('#bayar').val()); 
            var kembali = bayar - total;

            $("#kembalian").val(kembali);

            if (kembali < 0  || $("#kembalian").val() == '') {
              $("btnBayar").attr('disabled', 'disabled');
            }else{
              $("btnBayar").removeAttr('disabled');
            }
          });

    </script>
@endsection