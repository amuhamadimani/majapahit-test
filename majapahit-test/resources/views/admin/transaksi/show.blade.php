@extends('layouts.admin.master')

@section('content')
    <!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Transaksi | Point : <span style="color: red">{{ \Auth::user()->points }}</span></h1>
		<div class="form-group">
			<a href="{{ route('point.index') }}" class="btn btn-sm btn-warning shadow-sm btn-responsive"><i class="fas fa-certificate"></i> Tukar Point</a>
			<a href="{{ route('order.index') }}" class="btn btn-sm btn-primary shadow-sm btn-responsive"><i class="fas fa-shopping-cart"></i> Order</a>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-md-12">
			@include('pages-message.form-submit')
			@include('pages-message.notify-msg-error')
			@include('pages-message.notify-msg-success')
		</div>
		<div class="col-md-12">
		  <div class="card shadow">
		    <div class="card-header py-3">
			  <h6 class="m-0 font-weight-bold text-primary">Detail Transaksi</h6>
			  <a href="{{ route('transaksi.index') }}" class="btn btn-sm btn-primary shadow-sm btn-responsive float-right"><i class="fas fa-arrow-circle-left fa-sm text-white-50"></i> Kembali</a>
			  
		    </div>
		    <div class="card-body" style="overflow: auto;">
				<table class="table table-hover" id="listdata">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">ID Order</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Qty</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($order as $val)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $val->id_order }}</td>
                                <td>{{ getItem($val->id_item)->nama_item }}</td>
                                <td>{{ $val->qty }}</td>
                            </tr>
                        @endforeach
                      </tbody>
                  </table>
		    </div>
		  </div>
		</div>
	</div>
@endsection