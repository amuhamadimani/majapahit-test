@extends('layouts.admin.master')

@section('content')
    <!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">User Edit</h1>
		<a href="{{ route('user.index') }}" class="btn btn-sm btn-primary shadow-sm btn-responsive"><i class="fas fa-arrow-circle-left fa-sm text-white-50"></i> Kembali</a>
	</div>

	<div class="row mb-4">
		<div class="col-md-12">
		  <div class="card shadow">
		    <div class="card-header py-3">
		      <h6 class="m-0 font-weight-bold text-primary">Edit Data User</h6>
		    </div>
		    <div class="card-body">
		    	<div class="row">
					<div class="col-md-12">
						@include('pages-message.form-submit')
						@include('pages-message.notify-msg-error')
						@include('pages-message.notify-msg-success')
						<form action="{{ url('admin/user/update/'.$user->id ) }}" method="post" id="form-user">
							@csrf
							<div class="row">
								<div class="col-md-12">
							
									<div class="form-group">
										<label>Nama <span class="text-danger">*</span></label>
										<input type="text" class="form-control col-md-6" data-msg-required="Wajib diisi" required name="nama" value="{{ $user->name }}">
									</div>
									<div class="form-group">
										<label>Email <span class="text-danger">*</span></label>
										<input type="email" class="form-control col-md-6" data-msg-required="Wajib diisi" required name="email" value="{{ $user->email }}">
                                    </div>
									<div class="form-group">
										<label>Password <span class="text-danger">*</span></label>
                                        <input type="password" class="form-control col-md-6" id="password" data-msg-required="Wajib diisi" required name="password" value="{{ $user->password }}">
                                        <input type="checkbox" onclick="showPassword()">Show Password
									</div>
									<div class="form-group">
										<label>Status <span class="text-danger">*</span></label>
                                        <select name="status" id="" class="form-control col-md-6" data-msg-required="Wajib diisi" required>
                                            <option value="{{ $user->status }}" @if($user->status) selected @endif>
                                                @php
                                                    if ($user->status == 1) {
                                                        echo "Admin";
                                                    }else{
                                                        echo "User";
                                                    }
                                                @endphp
                                            </option>
                                            <option value="1">Admin</option>
                                            <option value="2">User</option>
                                        </select>
                                    </div>
									<div class="form-group">
										<input type="submit" class="btn btn-success" value="Update">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
		      
		    </div>
		  </div>
		</div>
	</div>
@endsection

@section('js')
    <script>
        function showPassword() {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
    </script>
@endsection