@extends('layouts.admin.master')

@section('content')
    <!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">User</h1>
		<a href="{{ route('user.create') }}" class="btn btn-sm btn-primary shadow-sm btn-responsive"><i class="fas fa-plus fa-sm text-white-50"></i> Create</a>
	</div>

	<div class="row mb-4">
		<div class="col-md-12">
			@include('pages-message.form-submit')
			@include('pages-message.notify-msg-error')
			@include('pages-message.notify-msg-success')
		</div>
		<div class="col-md-12">
		  <div class="card shadow">
		    <div class="card-header py-3">
		      <h6 class="m-0 font-weight-bold text-primary">Data User</h6>
		    </div>
		    <div class="card-body" style="overflow: auto;">
		      <table class="table table-hover" id="listdata">
		        <thead>
		            <tr>
		             	<th scope="col">No</th>
						<th scope="col">Nama</th>
						<th scope="col">Email</th>
						<th scope="col">Points</th>
						<th scope="col">Status</th>
						<th scope="col">Action</th>
		            </tr>
                  </thead>
                  <tbody>
                      @foreach ($user as $val)
                          <tr>
                              <td>{{ $loop->index+1 }}</td>
                              <td>{{ $val->name }}</td>
                              <td>{{ $val->email }}</td>
                              <td>
                                  @php
                                      if ($val->points == null) {
                                          echo "0";
                                      }else{
                                          echo $val->points;
                                      }
                                  @endphp
                              </td>
                              <td>
                                  @php
                                      if($val->status == 1){
                                          echo "Admin";
                                      }else{
                                          echo "User";
                                      }
                                  @endphp
                              </td>
                              <td>
                                  <a href="{{ url('admin/user/edit/'.$val->id) }}" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                  <a href="{{ url('admin/user/destroy/'.$val->id) }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                              </td>
                          </tr>
                      @endforeach
                  </tbody>
		      </table>
		    </div>
		  </div>
		</div>
	</div>
@endsection