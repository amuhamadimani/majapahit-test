@extends('layouts.admin.master')

@section('content')
    <!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
	</div>

	<div class="row mb-4">
		<div class="col-md-12">
		  <div class="card shadow">
            <div class="card-body" style="overflow: auto;">
                <div class="row">
                    <div class="col-md-4">
                        <a href="{{ route('user.index') }}" class="btn btn-primary btn-lg btn-block"><i class="fas fa-users"></i> Data User</a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ route('item.index') }}" class="btn btn-info btn-lg btn-block"><i class="fas fa-list"></i> Data Items</a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ route('transaksi.index') }}" class="btn btn-warning btn-lg btn-block"><i class="fas fa-shopping-cart"></i> Data Transaksi</a>
                    </div>
                </div>
		    </div>
		  </div>
		</div>
	</div>
@endsection