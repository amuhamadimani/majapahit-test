@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Error </strong>Ada beberapa masalah dengan input Anda.<br /><br />
    <ul style="list-style-type: circle;list-style-position: inside;">
      @foreach ($errors->all() as $error)
      <li>{{ ucfirst($error) }}</li>
      @endforeach
    </ul>
  </div>
@endif