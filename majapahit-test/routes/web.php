<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@index')->name('customer_table.index');

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@index')->name('user.index');
        Route::get('/create', 'UserController@create')->name('user.create');
        Route::post('/create', 'UserController@store')->name('user.store');
        Route::get('/edit/{id}', 'UserController@edit')->name('user.edit');
        Route::post('/update/{id}', 'UserController@update')->name('user.update');
        Route::get('/destroy/{id}', 'UserController@destroy')->name('user.destroy');
    });

    Route::group(['prefix' => 'item'], function () {
        Route::get('/', 'ItemController@index')->name('item.index');
        Route::get('/create', 'ItemController@create')->name('item.create');
        Route::post('/create', 'ItemController@store')->name('item.store');
        Route::get('/edit/{id}', 'ItemController@edit')->name('item.edit');
        Route::post('/update/{id}', 'ItemController@update')->name('item.update');
        Route::get('/destroy/{id}', 'ItemController@destroy')->name('item.destroy');
    });
    
    Route::group(['prefix' => 'transaksi'], function () {
        Route::get('/', 'TransaksiController@index')->name('transaksi.index');
        Route::get('/show/{id}', 'TransaksiController@show')->name('transaksi.show');

        Route::group(['prefix' => 'order'], function () {
            Route::get('/', 'OrderController@index')->name('order.index');
            Route::get('/{id}', 'OrderController@keranjang')->name('order.keranjang');
            Route::get('/update/{id}', 'OrderController@update')->name('order.update');
            Route::post('/store', 'OrderController@store')->name('order.store');
        });
        
        Route::group(['prefix' => 'point'], function () {
            Route::get('/', 'PointController@index')->name('point.index');
            Route::get('/{id}', 'PointController@keranjang')->name('point.keranjang');
            Route::get('/update/{id}', 'PointController@update')->name('point.update');
            Route::post('/store', 'PointController@store')->name('point.store');
        });
    });
    
    
});
